#ifndef FANTSU_H
#define FANTSU_H
#include <QObject>

class Fantsu : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString version READ readVersion NOTIFY versionChanged())

public:
    explicit Fantsu(QObject *parent = 0);
    ~Fantsu();

    Q_INVOKABLE void saveScreenCapture();

    QString readVersion();

signals:

    void versionChanged();

};


#endif // FANTSU_H

