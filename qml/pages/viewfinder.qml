import QtQuick 2.0
import Sailfish.Silica 1.0
import fantsucam.Fantsu 1.0
import QtMultimedia 5.0
import Sailfish.Media 1.0

Page
{
    id: page

    property string name
    property string glsl
    property bool showSlider

    SilicaFlickable
    {
        anchors.fill: parent

        Camera
        {
            id: camera
            flash.mode: Camera.FlashOff
            captureMode: Camera.CaptureStillImage
            focus.focusMode: Camera.FocusContinuous
            onError:
            {
                console.error("error: " + camera.errorString);
            }
        }

        PageHeader
        {
            anchors.top: parent.top
            id: cameraHeader
            title: "FantsuCam " + name
        }


        GStreamerVideoOutput
        {
            id: videoPreview
            z: 1
            anchors.centerIn: parent
            source: camera
            width: 480
        }

        ShaderEffect
        {
            property variant source: ShaderEffectSource { sourceItem: videoPreview; hideSource: true }
            // For wiggler
            property real wiggleAmount: divSlider.value/10

            // For trippy
            property real angle: 0.0
            NumberAnimation on angle { id: angleAnim; from: 0.0; to: 360.0; duration: 1000; loops: Animation.Infinite; }

            // For toon
            property real targetSize: 150
            property real resS: targetSize
            property real resT: targetSize
            property real magTol: 0.3
            property real quantize: 6.0 + (4.0 * divSlider.value)

            // For pixelate
            property real granularity: 5+(20*divSlider.value)

            anchors.fill: videoPreview

            fragmentShader: glsl
        }

        Slider
        {
            id: divSlider
            width: parent.width - 10
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: saveButton.top
            minimumValue: 0.0
            maximumValue: 1.0
            value: 0.5
            stepSize: 0.01
            visible: !saveTimer.running && showSlider
            onValueChanged: angleAnim.duration = 500+(divSlider.value*1000)
        }

        Button
        {
            id: saveButton
            visible: !saveTimer.running
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            text: "save"
            onClicked: saveTimer.start()
        }

        Timer
        {
            id: saveTimer
            interval: 500
            repeat: false
            running: false
            onTriggered:
            {
                fantsu.saveScreenCapture()
            }
        }

        Fantsu
        {
            id: fantsu
        }
    }
}


