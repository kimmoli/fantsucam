
import QtQuick 2.0
import Sailfish.Silica 1.0
import fantsucam.Fantsu 1.0


Page
{
    id: page

    SilicaFlickable
    {
        anchors.fill: parent

        PullDownMenu
        {
            MenuItem
            {
                text: "About"
                onClicked: pageStack.push(Qt.resolvedUrl("aboutPage.qml"),
                                          { "version": fantsu.version, "year": "2014", "name": "FantsuCam" } )
            }
        }

        contentHeight: column.height

        Column
        {
            id: column

            width: page.width
            spacing: Theme.paddingLarge
            PageHeader
            {
                title: "FantsuCam"
            }
            Button
            {
                /* http://qt-project.org/doc/qt-5/qtmultimedia-video-qmlvideofx-qml-qmlvideofx-effectselectionpanel-qml.html */
                x: Theme.paddingLarge
                text: "Wiggler"
                anchors.horizontalCenter: parent.horizontalCenter
                onClicked: pageStack.push(Qt.resolvedUrl("viewfinder.qml"), { "name": "Wiggler",
                "showSlider": true,
                "glsl": "
                    varying highp vec2 qt_TexCoord0;
                    uniform sampler2D source;
                    uniform highp float wiggleAmount;
                    void main(void)
                    {
                        highp vec2 wiggledTexCoord = qt_TexCoord0;
                        wiggledTexCoord.s += sin(4.0 * 3.141592653589 * wiggledTexCoord.t) * wiggleAmount;
                        gl_FragColor = texture2D(source, wiggledTexCoord.st);
                    }  "
                } )
            }
            Button
            {
                /* http://ilkka.github.io/blog/2011/03/04/qtquick_2_scenegraph_glsl_fragment_shader_tutorial/ */
                x: Theme.paddingLarge
                text: "Trippy"
                anchors.horizontalCenter: parent.horizontalCenter
                onClicked: pageStack.push(Qt.resolvedUrl("viewfinder.qml"), { "name": "Trippy",
                "showSlider": false,
                "glsl": "
                    varying highp vec2 qt_TexCoord0;
                    uniform lowp sampler2D source;
                    uniform highp float angle;
                    void main() {
                        highp float texAngle = 0.0;
                        if (qt_TexCoord0.x != 0.0 || qt_TexCoord0.y != 0.0) {
                            texAngle = atan(qt_TexCoord0.y - 0.5, qt_TexCoord0.x - 0.5);
                        }
                        highp float skew = sqrt(pow(qt_TexCoord0.x - 0.5, 2.0)
                                                + pow(qt_TexCoord0.y - 0.5, 2.0))
                                           * 10.0;
                        highp vec4 colorwheel = vec4(sin(texAngle + radians(angle) - skew),
                                               sin(texAngle + radians(angle - 120.0) - skew),
                                               sin(texAngle + radians(angle - 240.0) - skew),
                                               1.0);
                        highp float wavefactor = 0.03;
                        highp float wave_x = qt_TexCoord0.x + wavefactor
                                       * sin(radians(angle + qt_TexCoord0.x * 360.0));
                        highp float wave_y = qt_TexCoord0.y + wavefactor
                                       * cos(radians(angle + qt_TexCoord0.y * 360.0));
                        highp vec4 texpixel = texture2D(source, vec2(wave_x, wave_y));
                        gl_FragColor = colorwheel * texpixel;
                    }"
                    } )
                }
                Button
                {
                    /* http://ilkka.github.io/blog/2011/03/04/qtquick_2_scenegraph_glsl_fragment_shader_tutorial/ */
                    x: Theme.paddingLarge
                    text: "Wawwy"
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked: pageStack.push(Qt.resolvedUrl("viewfinder.qml"), { "name": "Wawwy",
                    "showSlider": true,
                    "glsl": "
                        varying highp vec2 qt_TexCoord0;
                        uniform lowp sampler2D source;
                        uniform highp float angle;
                        void main() {
                            highp float wavefactor = 0.03;
                            highp float wave_x = qt_TexCoord0.x + wavefactor
                                           * sin(radians(angle + qt_TexCoord0.x * 360.0));
                            highp float wave_y = qt_TexCoord0.y + wavefactor
                                           * cos(radians(angle + qt_TexCoord0.y * 360.0));
                            highp vec4 texpixel = texture2D(source, vec2(wave_x, wave_y));
                            gl_FragColor = texpixel;
                        }"
                    } )
                }
                Button
                {
                    /* http://qt-project.org/doc/qt-5/qtmultimedia-video-qmlvideofx-qml-qmlvideofx-effectselectionpanel-qml.html */
                    x: Theme.paddingLarge
                    text: "Toon"
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked: pageStack.push(Qt.resolvedUrl("viewfinder.qml"), { "name": "Toon",
                    "showSlider": true,
                    "glsl": "
                        uniform float threshold;
                        uniform float resS;
                        uniform float resT;
                        uniform float magTol;
                        uniform float quantize;

                        uniform sampler2D source;
                        uniform lowp float qt_Opacity;
                        varying vec2 qt_TexCoord0;

                        void main()
                        {
                              vec4 color = vec4(1.0, 0.0, 0.0, 1.1);
                              vec2 uv = qt_TexCoord0.xy;

                              vec2 st = qt_TexCoord0.st;
                              vec3 rgb = texture2D(source, st).rgb;
                              vec2 stp0 = vec2(1.0/resS,  0.0);
                              vec2 st0p = vec2(0.0     ,  1.0/resT);
                              vec2 stpp = vec2(1.0/resS,  1.0/resT);
                              vec2 stpm = vec2(1.0/resS, -1.0/resT);
                              float i00 =   dot( texture2D(source, st).rgb, vec3(0.2125,0.7154,0.0721));
                              float im1m1 = dot( texture2D(source, st-stpp).rgb, vec3(0.2125,0.7154,0.0721));
                              float ip1p1 = dot( texture2D(source, st+stpp).rgb, vec3(0.2125,0.7154,0.0721));
                              float im1p1 = dot( texture2D(source, st-stpm).rgb, vec3(0.2125,0.7154,0.0721));
                              float ip1m1 = dot( texture2D(source, st+stpm).rgb, vec3(0.2125,0.7154,0.0721));
                              float im10 =  dot( texture2D(source, st-stp0).rgb, vec3(0.2125,0.7154,0.0721));
                              float ip10 =  dot( texture2D(source, st+stp0).rgb, vec3(0.2125,0.7154,0.0721));
                              float i0m1 =  dot( texture2D(source, st-st0p).rgb, vec3(0.2125,0.7154,0.0721));
                              float i0p1 =  dot( texture2D(source, st+st0p).rgb, vec3(0.2125,0.7154,0.0721));
                              float h = -1.*im1p1 - 2.*i0p1 - 1.*ip1p1  +  1.*im1m1 + 2.*i0m1 + 1.*ip1m1;
                              float v = -1.*im1m1 - 2.*im10 - 1.*im1p1  +  1.*ip1m1 + 2.*ip10 + 1.*ip1p1;
                              float mag = sqrt(h*h + v*v);
                              if (mag > magTol) {
                                  color = vec4(0.0, 0.0, 0.0, 1.0);
                              }
                              else {
                                  rgb.rgb *= quantize;
                                  rgb.rgb += vec3(0.5, 0.5, 0.5);
                                  ivec3 irgb = ivec3(rgb.rgb);
                                  rgb.rgb = vec3(irgb) / quantize;
                                  color = vec4(rgb, 1.0);
                              }
                              gl_FragColor = qt_Opacity * color;
                          } "


                    } )
                }
                Button
                {
                    x: Theme.paddingLarge
                    text: "Pixelate"
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked: pageStack.push(Qt.resolvedUrl("viewfinder.qml"), { "name": "Pixelate",
                    "showSlider": true,
                    "glsl": "
                        uniform float granularity;

                        uniform sampler2D source;
                        uniform lowp float qt_Opacity;
                        varying vec2 qt_TexCoord0;

                        void main()
                        {
                            vec2 uv = qt_TexCoord0.xy;
                            vec2 tc = qt_TexCoord0;

                            float dx = granularity / 480.0;
                            float dy = granularity / 640.0;
                            tc = vec2(dx*(floor(uv.x/dx) + 0.5),
                                      dy*(floor(uv.y/dy) + 0.5));

                            gl_FragColor = qt_Opacity * texture2D(source, tc);
                        } "
                    } )
                }
                Button
                {
                    x: Theme.paddingLarge
                    text: "Warhol"
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked: pageStack.push(Qt.resolvedUrl("viewfinder.qml"), { "name": "Warhol",
                    "showSlider": false,
                    "glsl": "
                        uniform sampler2D source;
                        uniform lowp float qt_Opacity;
                        varying vec2 qt_TexCoord0;

                        void main()
                        {
                          vec2 uv = qt_TexCoord0.xy;
                          vec4 orig = texture2D(source, uv);
                          vec3 col = orig.rgb;
                          float y = 0.3 *col.r + 0.59 * col.g + 0.11 * col.b;
                          y = y < 0.3 ? 0.0 : (y < 0.6 ? 0.5 : 1.0);
                          if (y == 0.5)
                              col = vec3(0.8, 0.0, 0.0);
                          else if (y == 1.0)
                              col = vec3(0.9, 0.9, 0.0);
                          else
                              col = vec3(0.0, 0.0, 0.0);

                          gl_FragColor = qt_Opacity * vec4(col, 1.0);
                        }
                        "
                    } )
                }


        }
    }

    Fantsu
    {
        id: fantsu
    }

}

